import { createContext, useReducer } from "react";

import { UserType, userInitialState, userReducer } from '../reducers/userReducer'
import {ThemerType, themeInitialState,themeReducer} from '../reducers/themeReducer'
import { reducerActionType } from "../Types/reducerActionType";

type initialStateType = {
    user: UserType;
    theme: ThemerType;
}

const initialState = {
    user: userInitialState,
    theme: themeInitialState
}

type contextType ={
    state: initialStateType;
    dispatch: React.Dispatch<any>;
}




export const Context = createContext<contextType>({
    state: initialState,
    dispatch: () => null
})

const mainReducer = ( state: initialStateType , action: reducerActionType ) => ({
    user: userReducer(state.user, action),
    theme: themeReducer(state.theme, action)
})

export const ContextProvider:  React.FC<{children: React.ReactNode}> = ({children}) => {
    const [state, dispatch] = useReducer(mainReducer, initialState);
    return (
        <Context.Provider value={{state, dispatch }}>
            {children}
        </Context.Provider>
    )
}