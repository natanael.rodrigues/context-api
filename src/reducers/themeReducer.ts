import { reducerActionType } from '../Types/reducerActionType';

export type ThemerType = {
  status: 'dark' | 'light';
};

export const themeInitialState: ThemerType = {
  status:'light'
};

export const themeReducer = (state: ThemerType, action: reducerActionType) => {
  switch (action.type) {
    case 'CHANGE_STATUS':
      return { ...state, status: action.payload.status };
      break;
  }

  return state;
};
