import React, { useContext, useState } from "react"

import { Context } from "../contexts/Context"

import { Link } from "react-router-dom"

export const SignUp = () =>{
    const { state, dispatch } = useContext(Context)
    const [nameInput, setNameInput] = useState('')
    const [ageInput, setAgeInput] = useState(0)

    const handleSave = () => {
        dispatch({
            type: 'CHANGE_NAME',
            payload:{
                name: nameInput
            }
        })
        dispatch({
            type: 'CHANGE_AGE',
            payload:{
                age: ageInput
            }
        })
    }

    const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setNameInput(e.target.value)
    }

    const handleAgeNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAgeInput(parseInt(e.target.value))
    }

    return (
        <>
            <h3>SignUp</h3>
            <br/>
            <input 
                type='text'
                placeholder="Digite um nome"
                value={nameInput}
                onChange={handleNameChange}
            
            />
            <br/>
            <input 
                type='text'
                placeholder="Digite um nome"
                value={ageInput}
                onChange={handleAgeNameChange}
            
            />
            <br/>
            <button onClick={handleSave}>Salvar</button>
            <br/>
            <Link to="/exibir">Ir para o ShowData</Link>
        </>
    )
}