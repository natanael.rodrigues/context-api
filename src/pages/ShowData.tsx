import { useContext } from "react"
import { Context } from "../contexts/Context"


import { Link } from "react-router-dom"

export const ShowData = () =>{
    
    const { state, dispatch } = useContext(Context)

    return (
        <>
            <h1>ShowData</h1>
            Nome: {state.user.name} <br />
            Idade: {state.user.age}<br/><br/>
            <Link to="/">Voltar</Link>
        </>
    )
}